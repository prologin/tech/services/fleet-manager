#!/bin/sh

RAND_SECRET_NAMES="django-secret-key postgres-passwd postgres-kea-passwd postgres-powerdns-passwd postgres-salt-passwd kea-shared-secret salt-api-shared-secret"
RSA_KEYPAIR_NAMES="salt-master"

for name in ${RAND_SECRET_NAMES}; do
	DST=./secrets/$name
	if [ ! -e "${DST}" ]; then
		tr -dc 'a-zA-Z0-9' < /dev/urandom | fold -w 32 | head -n 1 \
			> "${DST}"
	fi
done

# The following secrets have hardcoded values and are not modifiable for now
echo accessKey1 > ./secrets/s3-access
echo verySecretKey1 > ./secrets/s3-secret

echo "tVOgJ4hxT8uV0bjMIjT4psPARaR1XgxhIYoXcqnGIFZVmCpMTrhDKEK2qy6TT9VO" > ./secrets/oidc-client-id
echo "t9pvgKnwB37CeutWpPOEKMDv3gmp9kjUtQ5ifDzF9gBPMGO4yhHLdUNnh7AjRuWH" > ./secrets/oidc-client-secret

for name in ${RSA_KEYPAIR_NAMES}; do
	DST=./secrets/$name
	openssl genrsa -out "${DST}.pem" 2048
	openssl rsa -in "${DST}.pem" -outform PEM -pubout -out "${DST}.pub"
done
