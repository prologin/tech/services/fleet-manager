from datetime import timedelta

from django.contrib.postgres.fields import DateTimeRangeField
from django.db import models
from django.db.models import Q
from django.utils import timezone
from psycopg2.extras import DateTimeTZRange

from fleet.models import Workstation


class SessionBaseQuerySet(models.QuerySet):
    def by_room(self, room):
        return self.filter(
            ip__in=map(
                str, Workstation.objects.filter(room=room).values_list("ip", flat=True)
            )
        )

    def latest_by_login(self):
        latest_ids = []
        for login in set(self.values_list("login", flat=True)):
            latest_ids.append(self.filter(login=login).latest().id)
        return self.filter(id__in=latest_ids)


class SessionBase(models.Model):
    SESSION_TIMEOUT_SECONDS = 3 * 60

    id = models.BigAutoField(primary_key=True, unique=True)

    ip = models.CharField(max_length=255, db_index=True)

    login = models.CharField(max_length=255, db_index=True)

    image = models.CharField(max_length=255)

    objects = SessionBaseQuerySet.as_manager()

    @property
    def workstation(self):
        try:
            return Workstation.objects.get(ip=self.ip)
        except Workstation.DoesNotExist:
            return None

    class Meta:
        abstract = True


class SessionStateQuerySet(SessionBaseQuerySet):
    def current(self):
        timeout = timedelta(seconds=self.model.SESSION_TIMEOUT_SECONDS)
        return self.filter(timestamp__gte=timezone.now() - timeout)


class SessionState(SessionBase):
    timestamp = models.DateTimeField(db_index=True)

    locked = models.BooleanField(default=False)

    objects = SessionStateQuerySet.as_manager()

    class Meta:
        get_latest_by = "timestamp"
        ordering = ("-timestamp",)

    def __str__(self):
        return f"{self.timestamp} {self.ip} {self.login} {self.image}"

    def is_same_session(self, other):
        timeout = timedelta(seconds=self.SESSION_TIMEOUT_SECONDS)
        return all(
            (self.login == other.login, self.ip == other.ip, self.image == other.image)
        ) and any(
            (
                self.timestamp - timeout <= other.timestamp <= self.timestamp + timeout,
                other.timestamp - timeout
                <= self.timestamp
                <= other.timestamp + timeout,
            )
        )

    @classmethod
    def connected_users(cls):
        return cls.objects.filter(
            timestamp__gte=timezone.now()
            - timedelta(seconds=cls.SESSION_TIMEOUT_SECONDS)
        )


class Session(SessionBase):
    interval = DateTimeRangeField(db_index=True)

    class Meta:
        get_latest_by = "interval"
        ordering = ("-interval", "login")

    def __str__(self):
        return f"{self.interval} {self.ip} {self.login} {self.image}"

    def merge_session(self, other):
        self.interval = DateTimeTZRange(
            lower=min(self.interval.lower, other.interval.lower),
            upper=max(self.interval.upper, other.interval.upper),
        )

    @classmethod
    def prepare_merge_sessions(cls, sessions):
        timeout = timedelta(seconds=cls.SESSION_TIMEOUT_SECONDS)
        sessions_to_save = []

        for session in sessions:
            existing_sessions = cls.objects.filter(
                login=session.login,
                ip=session.ip,
                image=session.image,
            ).filter(
                Q(interval__contains=session.interval.lower - timeout)
                | Q(interval__contains=session.interval.upper + timeout),
            )

            if existing_sessions:
                existing_session = existing_sessions[0]
                existing_session.merge_session(session)
                session = existing_session

            sessions_to_save.append(session)

        return sessions_to_save
