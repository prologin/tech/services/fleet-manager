import ipaddress

from celery import shared_task
from datetime import timedelta
from django.conf import settings
from django.db import transaction
from django.utils import timezone
from psycopg2.extras import DateTimeTZRange
from salt.backend import get_salt_backend
from salt.models import Minion

from . import models

salt = get_salt_backend()


@shared_task
def sessions_merge():
    timeout = timedelta(seconds=settings.SESSIONS_MERGE_TIMEOUT_SECONDS)
    qs = models.SessionState.objects.filter(
        timestamp__lt=timezone.now() - timeout
    ).order_by("ip", "login", "timestamp")

    if not qs:
        return

    last_sessions = []

    def new_session(last_states):
        first_state = last_states[0]
        last_state = last_states[-1]
        return models.Session(
            ip=first_state.ip,
            login=first_state.login,
            image=first_state.image,
            interval=DateTimeTZRange(
                lower=first_state.timestamp,
                upper=last_state.timestamp + timedelta(seconds=1),
            ),
        )

    last_states = []
    for entry in qs:
        if last_states and not last_states[-1].is_same_session(entry):
            last_sessions.append(new_session(last_states))
            last_states = []
        last_states.append(entry)
    last_sessions.append(new_session(last_states))

    sessions_to_save = models.Session.prepare_merge_sessions(last_sessions)

    with transaction.atomic():
        for session in sessions_to_save:
            session.save()
        qs.delete()


def is_ignored_ip(ip):
    ip = ipaddress.ip_address(ip)
    for network in map(ipaddress.ip_network, settings.SESSIONS_IGNORE_SUBNETS):
        if ip in network:
            return True
    return False


@shared_task
def sessions_discover():
    for minion_id in salt.list_minions():
        try:
            minion = Minion.objects.get(minion_id=minion_id)
        except Minion.DoesNotExist:
            minion = Minion.populate(minion_id)

        sessions = salt.cmd_run(minion_id, "loginctl list-sessions")
        if not minion_id in sessions:
            continue
        # Skip first line, its a header
        # Skip last two lines, one is empty, the other is a footer
        sessions = sessions[minion_id].strip().split("\n")[1:-2]
        users = {session.strip().split()[2] for session in sessions}

        now = timezone.now()

        for ip in minion.grains["ipv4"]:
            if is_ignored_ip(ip):
                continue

            for user in users:
                if user in settings.SESSIONS_IGNORE_USERS:
                    continue

                models.SessionState(
                    timestamp=now,
                    ip=ip,
                    login=user,
                    image=minion.grains["image"],
                ).save()
