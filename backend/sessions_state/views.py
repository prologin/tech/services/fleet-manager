from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .filters import (
    SessionAnonymousFilter,
    SessionFilter,
    SessionStateAnonymousFilter,
    SessionStateFilter,
)
from .models import Session, SessionState
from .permissions import DjangoModelWithViewPermissions, SessionStatePermissions
from .serializers import (
    SessionSerializer,
    SessionStateAnonymousSerializer,
    SessionStateSerializer,
    SesssionStatsSerializer,
)


class SessionStateViewSet(ModelViewSet):
    queryset = SessionState.objects.all()
    serializer_class = SessionStateSerializer
    permission_classes = [IsAuthenticated & SessionStatePermissions]

    # pylint:disable=unused-argument
    def get_filterset_class(self, queryset=None):
        if self.request.user.has_perm("sessions_state.view_sessionstate"):
            return SessionStateFilter
        return SessionStateAnonymousFilter

    def get_serializer_class(self):
        if self.request.user.has_perm("sessions_state.view_sessionstate"):
            return SessionStateSerializer
        return SessionStateAnonymousSerializer


class SessionViewSet(ModelViewSet):
    queryset = Session.objects.all()
    serializer_class = SessionSerializer
    permission_classes = [DjangoModelWithViewPermissions]

    # pylint:disable=unused-argument
    def get_filterset_class(self, queryset=None):
        if self.request.user.has_perm("sessions_state.view_session"):
            return SessionFilter
        return SessionAnonymousFilter

    @action(detail=False, methods=["get"], permission_classes=[IsAuthenticated])
    def stats(self, request):
        queryset = self.filter_queryset(self.get_queryset())

        stats = {}
        for session in queryset:
            if session.ip not in stats:
                stats[session.ip] = 0
            stats[session.ip] += int(
                (session.interval.upper - session.interval.lower).seconds / 60
            )

        return Response(
            SesssionStatsSerializer(
                [{"ip": ip, "duration": duration} for ip, duration in stats.items()],
                many=True,
            ).data
        )
