from django.contrib import admin

from . import models


@admin.register(models.SessionState)
class SessionStateAdmin(admin.ModelAdmin):
    list_display = ("login", "ip", "image", "timestamp")
    list_display_links = list_display
    list_filter = ("login", "ip", "image")
    search_fields = ("login", "ip", "image")


@admin.register(models.Session)
class SessionAdmin(admin.ModelAdmin):
    list_display = ("login", "ip", "image", "interval")
    list_display_links = list_display
    list_filter = ("login", "ip", "image")
    search_fields = ("login", "ip", "image")
