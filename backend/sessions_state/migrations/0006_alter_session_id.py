# Generated by Django 3.2.9 on 2021-11-07 21:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("sessions_state", "0005_alter_sessionstate_options"),
    ]

    operations = [
        migrations.AlterField(
            model_name="session",
            name="id",
            field=models.BigAutoField(primary_key=True, serialize=False, unique=True),
        ),
    ]
