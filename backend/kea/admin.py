from django.contrib import admin

from .models import (
    Lease4,
    Lease4Stat,
    LeaseHwaddrSource,
    LeaseState,
    Logs,
    SchemaVersion,
)


@admin.register(Lease4, Lease4Stat, LeaseHwaddrSource, LeaseState, Logs, SchemaVersion)
class KeaAdmin(admin.ModelAdmin):
    pass
