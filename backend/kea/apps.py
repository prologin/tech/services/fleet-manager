from django.apps import AppConfig


class KeaConfig(AppConfig):
    name = "kea"
