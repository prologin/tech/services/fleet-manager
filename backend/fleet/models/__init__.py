from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from modelsync import SyncedModelMixin
from netfields import CidrAddressField, InetAddressField, MACAddressField, NetManager

from .sync import (
    DomainSyncAdapter,
    RecordASyncAdapter,
    RecordNSSyncAdapter,
    RecordSOASyncAdapter,
    RecordTXTSyncAdapter,
)


class Site(models.Model):
    name = models.CharField(max_length=255)
    domain = models.CharField(max_length=255, unique=True)

    class Meta:
        ordering = ("name",)

    def __str__(self):
        return self.name

    @property
    def zone(self):
        return self.domain


class DhcpSubnet(models.Model):
    subnet = CidrAddressField()

    router = InetAddressField(store_prefix_length=False)

    allocation_pool_start = InetAddressField(store_prefix_length=False)
    allocation_pool_end = InetAddressField(store_prefix_length=False)

    objects = NetManager()

    def __str__(self):
        return str(self.subnet)

    def clean(self):
        if (
            not self.allocation_pool_start  # pylint:disable=unneeded-not # noqa: E501
            < self.allocation_pool_end
        ):
            raise ValidationError(
                {
                    "allocation_pool_start": "allocation_pool_start should be before allocation_pool_end",  # noqa: E501
                    "allocation_pool_end": "allocation_pool_end should be after allocation_pool_start",  # noqa: E501
                }
            )
        if (
            self.allocation_pool_start
            not in self.subnet  # pylint:disable=unsupported-membership-test # noqa: E501
        ):
            raise ValidationError(
                {
                    "allocation_pool_start": f"allocation_pool_start should be in subnet {self.subnet}",  # noqa: E501
                }
            )
        if (
            self.allocation_pool_end
            not in self.subnet  # pylint:disable=unsupported-membership-test # noqa: E501
        ):
            raise ValidationError(
                {
                    "allocation_pool_end": f"allocation_pool_end should be in subnet {self.subnet}",  # noqa: E501
                }
            )
        if (
            self.router
            not in self.subnet  # pylint:disable=unsupported-membership-test # noqa: E501
        ):
            raise ValidationError(
                {
                    "router": f"router should be in subnet {self.subnet}",  # noqa: E501
                }
            )


class Room(SyncedModelMixin, models.Model):
    name = models.CharField(max_length=255)

    slug = models.SlugField(max_length=64)

    site = models.ForeignKey(Site, on_delete=models.CASCADE, db_index=True)

    dhcp_subnet = models.ForeignKey(DhcpSubnet, on_delete=models.RESTRICT)
    mapping_network = CidrAddressField()

    currently_mapping = models.BooleanField(default=False)

    map = models.TextField(default="", blank=True)

    objects = NetManager()

    class Meta:
        ordering = ("site", "name")
        unique_together = (("name", "site"),)

    class SyncMeta:
        sync_adapters = (DomainSyncAdapter, RecordSOASyncAdapter, RecordNSSyncAdapter)

    @classmethod
    def sync_all(cls, *, pretend=False):
        result = {}
        for adapter in cls.get_sync_adapters():
            result[adapter] = adapter.sync_all(models=(cls, Site), pretend=pretend)
        return result

    def __str__(self):
        return f"{self.site} - {self.name}"

    @property
    def domain(self):
        return f"{self.slug}.{self.site.domain}"

    @property
    def zone(self):
        return self.domain

    def clean(self):
        if not self.mapping_network.subnet_of(self.dhcp_subnet.subnet):
            raise ValidationError(
                {
                    "mapping_network": f"mapping_network should be part of dhcp_subnet ({self.dhcp_subnet})",  # noqa: E501
                }
            )

        if self.currently_mapping:
            try:
                tmp = Room.objects.get(currently_mapping=True)
                if self != tmp:
                    raise ValidationError(
                        {
                            "currently_mapping": f"Only one room can be mapped at a time. Currently, {tmp} is set as being mapped."  # noqa: E501
                        }
                    )
            except Room.DoesNotExist:
                pass

        super().clean()


class ExtraTxtRecordBase(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    content = models.CharField(max_length=65535, blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return f"{self.domain} TXT IN {self.content}"

    @property
    def domain(self):
        return f"{self.name}.{self.zone}"


class ExtraTxtRecordSite(ExtraTxtRecordBase):
    site = models.ForeignKey(Site, on_delete=models.CASCADE)

    @property
    def zone(self):
        return self.site.domain


class ExtraTxtRecordRoom(SyncedModelMixin, ExtraTxtRecordBase):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)

    class SyncMeta:
        sync_adapters = (RecordTXTSyncAdapter,)

    @property
    def zone(self):
        return self.room.domain

    @classmethod
    def sync_all(cls, *, pretend=False):
        result = {}
        for adapter in cls.get_sync_adapters():
            result[adapter] = adapter.sync_all(
                models=(cls, ExtraTxtRecordSite), pretend=pretend
            )
        return result


class Workstation(SyncedModelMixin, models.Model):
    name = models.CharField(max_length=255)

    room = models.ForeignKey(Room, on_delete=models.CASCADE, db_index=True)

    ip = InetAddressField(store_prefix_length=False, blank=True, db_index=True)

    switch_name = models.CharField(max_length=255, blank=True)

    switch_port = models.CharField(max_length=255, blank=True)

    added_at = models.DateTimeField(auto_now_add=True)

    exclude_from_mapping = models.BooleanField(default=False)

    objects = NetManager()

    class Meta:
        ordering = ("room", "name")
        unique_together = (
            ("name", "room"),
            ("ip", "room"),
            ("name", "switch_name", "switch_port", "room"),
        )

    class SyncMeta:
        sync_adapters = (RecordASyncAdapter,)

    @classmethod
    def sync_all(cls, *, pretend=False):
        result = {}
        for adapter in cls.get_sync_adapters():
            result[adapter] = adapter.sync_all(models=(cls,), pretend=pretend)
        return result

    def __str__(self):
        return f"{self.room} - {self.name} {self.ip}"

    def save(self, *args, **kwargs):
        if not self.ip:
            self.auto_assign_ip()
        super().save(*args, **kwargs)

    @property
    def fqdn(self):
        return f"{self.name}.{self.room.domain}"

    @property
    def zone(self):
        return self.room.domain

    @property
    def domain(self):
        return self.fqdn

    def clean(self):
        if not self.exclude_from_mapping:
            if (
                self.ip
                and self.ip
                not in self.room.mapping_network  # pylint:disable=unsupported-membership-test # noqa: E501
            ):
                raise ValidationError(
                    {
                        "ip": f"IP address should be part of room mapping network ({self.room.mapping_network})",  # noqa: E501
                    }
                )
        if (
            self.ip
            and self.ip
            not in self.room.dhcp_subnet.subnet  # pylint:disable=unsupported-membership-test # noqa: E501
        ):
            raise ValidationError(
                {
                    "ip": f"IP address should be part of room DHCP network ({self.room.dhcp_subnet.subnet})",  # noqa: E501
                }
            )

        super().clean()

    def auto_assign_ip(self):
        ip_choices = set(self.room.mapping_network.hosts()) - set(
            [m["ip"] for m in Workstation.objects.filter(room=self.room).values("ip")]
            + [self.room.dhcp_subnet.router]
        )
        if len(ip_choices) == 0:
            raise ValidationError(
                {
                    "ip": f"Cannot automatically assign IP. Mapping subnet ({self.room.mapping_network}) is full. Consider increasing its size."  # noqa: E501
                }
            )
        self.ip = ip_choices.pop()


class Machine(models.Model):
    workstation = models.OneToOneField(
        Workstation,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        unique=True,
        db_index=True,
    )

    mac = MACAddressField(unique=True, db_index=True)

    board_uuid = models.UUIDField(blank=True, null=True)

    board_serial = models.CharField(max_length=255, blank=True)

    added_at = models.DateTimeField(auto_now_add=True)

    objects = NetManager()

    class Meta:
        ordering = ("workstation",)

    def __str__(self):
        return f"{self.mac} {self.board_uuid} {self.board_serial}"


class Issue(models.Model):
    class ProblemType(models.TextChoices):
        UNKNOWN = "unknown", "Unknown"
        MACHINE = "machine", "Machine"
        KEYBOARD = "keyboard", "Keyboard"
        MOUSE = "mouse", "Mouse"
        SCREEN = "screen", "Screen"
        VIDEO = "video_cable", "Video cable"
        POWER = "power", "Power"
        NETWORK = "network", "Network"
        LINK = "link", "Network cable"
        TABLE = "table", "Table"
        VESA = "vesa", "VESA mount"

    class Severity(models.TextChoices):
        UNKNOWN = "unknown", "Unknown"
        LOW = "low", "Low"
        MEDIUM = "medium", "Medium"
        HIGH = "high", "High"

    class Status(models.TextChoices):
        NEW = "new", "New"
        CONFIRMED = "confirmed", "Confirmed"
        REJECTED = "rejected", "Rejected"
        RESOLVED = "resolved", "Resolved"

    workstation = models.ForeignKey(
        Workstation, on_delete=models.CASCADE, db_index=True
    )
    description = models.TextField()
    problem_type = models.CharField(
        max_length=100, choices=ProblemType.choices, default=ProblemType.UNKNOWN
    )
    severity = models.CharField(
        max_length=100, choices=Severity.choices, default=Severity.UNKNOWN
    )
    status = models.CharField(
        max_length=100, choices=Status.choices, default=Status.NEW
    )

    added_by = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name="+"
    )
    added_at = models.DateTimeField(auto_now_add=True)

    confirmed_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="+",
    )
    confirmed_at = models.DateTimeField(null=True, blank=True)

    rejected_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="+",
    )
    rejected_at = models.DateTimeField(null=True, blank=True)

    resolved_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="+",
        db_index=True,
    )
    resolved_at = models.DateTimeField(null=True, blank=True)

    last_updated = models.DateTimeField(auto_now=True)

    def confirm(self, user, save=False):
        self.status = Issue.Status.CONFIRMED
        self.confirmed_by = user
        self.confirmed_at = timezone.now()

        if save:
            self.save()

    def reject(self, user, save=False):
        self.status = Issue.Status.REJECTED
        self.rejected_by = user
        self.rejected_at = timezone.now()
        if save:
            self.save()

    def resolve(self, user, save=False):
        self.status = Issue.Status.RESOLVED
        self.resolved_by = user
        self.resolved_at = timezone.now()
        if save:
            self.save()
