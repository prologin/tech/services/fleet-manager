import django.db.models.deletion
import netfields.fields
from django.core.exceptions import ObjectDoesNotExist
from django.db import migrations, models


def populate_machine_workstation_field(apps, schema_editor):
    Machine = apps.get_model("fleet", "Machine")
    Workstation = apps.get_model("fleet", "Workstation")
    db_alias = schema_editor.connection.alias

    for machine in Machine.objects.using(db_alias).all():
        try:
            workstation = Workstation.objects.using(db_alias).get(
                name=machine.name, room=machine.room
            )
        except ObjectDoesNotExist:
            continue
        machine.workstation = workstation
        workstation.added_at = machine.added_at
        machine.save()
        workstation.save()


class Migration(migrations.Migration):

    dependencies = [
        ("fleet", "0002_alter_site_domain"),
    ]

    operations = [
        migrations.AlterField(
            model_name="machine",
            name="room",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="machine",
                to="fleet.room",
            ),
        ),
        migrations.CreateModel(
            name="Workstation",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=255)),
                ("mac", netfields.fields.MACAddressField(unique=True)),
                ("ip", netfields.fields.InetAddressField(blank=True, max_length=39)),
                ("board_uuid", models.UUIDField(blank=True)),
                ("board_serial", models.CharField(blank=True, max_length=255)),
                ("switch_name", models.CharField(blank=True, max_length=255)),
                ("switch_port", models.CharField(blank=True, max_length=255)),
                ("added_at", models.DateTimeField(auto_now_add=True)),
                ("exclude_from_mapping", models.BooleanField(default=False)),
                (
                    "room",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="workstation",
                        to="fleet.room",
                    ),
                ),
            ],
            options={
                "ordering": ("room", "name"),
            },
        ),
        migrations.RunSQL(
            sql="INSERT INTO fleet_workstation (id, name, mac, ip, board_uuid, board_serial, switch_name, switch_port, added_at, exclude_from_mapping, room_id) SELECT id, name, mac, ip, board_uuid, board_serial, switch_name, switch_port, added_at, exclude_from_mapping, room_id FROM fleet_machine;",
            reverse_sql="TRUNCATE fleet_workstation;",
            elidable=True,
        ),
        migrations.AddField(
            model_name="machine",
            name="workstation",
            field=models.OneToOneField(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="fleet.workstation",
            ),
        ),
        migrations.RunPython(
            code=populate_machine_workstation_field,
            reverse_code=migrations.RunPython.noop,
            elidable=True,
        ),
        migrations.AlterModelOptions(
            name="machine",
            options={"ordering": ("workstation",)},
        ),
        migrations.AlterUniqueTogether(
            name="machine",
            unique_together=set(),
        ),
        migrations.AlterUniqueTogether(
            name="workstation",
            unique_together={("name", "switch_name", "switch_port"), ("name", "room")},
        ),
        migrations.RemoveField(
            model_name="machine",
            name="exclude_from_mapping",
        ),
        migrations.RemoveField(
            model_name="machine",
            name="ip",
        ),
        migrations.RemoveField(
            model_name="machine",
            name="name",
        ),
        migrations.RemoveField(
            model_name="machine",
            name="room",
        ),
        migrations.RemoveField(
            model_name="machine",
            name="switch_name",
        ),
        migrations.RemoveField(
            model_name="machine",
            name="switch_port",
        ),
        migrations.RemoveField(
            model_name="workstation",
            name="board_serial",
        ),
        migrations.RemoveField(
            model_name="workstation",
            name="board_uuid",
        ),
        migrations.RemoveField(
            model_name="workstation",
            name="mac",
        ),
    ]
