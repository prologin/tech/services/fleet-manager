# Generated by Django 3.2.9 on 2021-11-06 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("fleet", "0005_alter_machine_board_uuid"),
    ]

    operations = [
        migrations.AddField(
            model_name="room",
            name="map",
            field=models.TextField(default=""),
        ),
    ]
