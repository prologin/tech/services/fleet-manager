from django.contrib import admin
from django.contrib.auth.decorators import login_required

from .models import (
    DhcpSubnet,
    ExtraTxtRecordRoom,
    ExtraTxtRecordSite,
    Issue,
    Machine,
    Room,
    Site,
    Workstation,
)

admin.site.login = login_required(admin.site.login)


class ExtraTxtRecordSiteInline(admin.TabularInline):
    model = ExtraTxtRecordSite


@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    list_display = ("name", "domain")
    list_display_links = list_display
    search_fields = list_display
    inlines = (ExtraTxtRecordSiteInline,)


@admin.register(DhcpSubnet)
class DhcpSubnetAdmin(admin.ModelAdmin):
    list_display = ("subnet", "router", "allocation_pool_start", "allocation_pool_end")
    list_display_links = list_display
    search_fields = list_display


class ExtraTxtRecordRoomInline(admin.TabularInline):
    model = ExtraTxtRecordRoom


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "slug",
        "site",
        "domain",
        "dhcp_subnet",
        "mapping_network",
        "currently_mapping",
    )
    list_display_links = list_display
    list_filter = ("site", "dhcp_subnet", "currently_mapping")
    search_fields = list_display
    inlines = (ExtraTxtRecordRoomInline,)


class MachineInline(admin.StackedInline):
    model = Machine


class IssueInline(admin.StackedInline):
    model = Issue
    extra = 1


@admin.register(Workstation)
class WorkstationAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "room",
        "ip",
        "switch_name",
        "switch_port",
    )
    list_display_links = list_display
    list_filter = ("room", "switch_name")
    search_fields = (
        "room__name",
        "room__slug",
        "name",
        "ip",
        "switch_name",
        "switch_port",
    )
    date_hierarchy = "added_at"
    inlines = (MachineInline, IssueInline)


@admin.register(Machine)
class MachineAdmin(admin.ModelAdmin):
    list_display = (
        "workstation",
        "mac",
        "board_uuid",
        "board_serial",
    )
    list_display_links = list_display
    search_fields = (
        "workstation__ip",
        "workstation__name",
        "workstation__room__name",
        "workstation__room__slug",
        "mac",
        "board_uuid",
        "board_serial",
    )
    date_hierarchy = "added_at"


@admin.register(Issue)
class IssueAdmin(admin.ModelAdmin):
    list_display = (
        "workstation",
        "problem_type",
        "severity",
        "status",
        "added_by",
        "added_at",
        "confirmed_by",
        "rejected_by",
        "resolved_by",
    )
    list_display_links = list_display
    list_filter = ("problem_type", "severity", "status")
    date_hierarchy = "added_at"
    actions = ("confirm", "reject", "resolve")

    @admin.display(description="Confirm")
    def confirm(self, request, queryset):
        for issue in queryset:
            issue.confirm(request.user, save=True)

    @admin.display(description="Reject")
    def reject(self, request, queryset):
        for issue in queryset:
            issue.reject(request.user, save=True)

    @admin.display(description="Resolve")
    def resolve(self, request, queryset):
        for issue in queryset:
            issue.resolve(request.user, save=True)
