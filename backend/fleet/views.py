from django.conf import settings
from django.core.exceptions import ValidationError
from django.http import HttpResponseForbidden, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, View
from rest_framework.decorators import action
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.viewsets import ModelViewSet

from .filters import IssueFilter
from .forms import RegisterForm
from .models import DhcpSubnet, Issue, Machine, Room, Site, Workstation
from .permissions import DjangoModelWithViewPermissions
from .serializers import (
    IssueSerializer,
    MachineSerializer,
    RoomSerializer,
    SiteSerializer,
    WorkstationSerializer,
)


class DhcpConfigDiscovery(View):
    http_method_names = ("get",)

    def get(self, request, *args, **kwargs):
        if (
            "Kea-Shared-Secret" not in request.headers
            or request.headers["Kea-Shared-Secret"] != settings.KEA_SHARED_SECRET
        ):
            return HttpResponseForbidden("Kea-Shared-Secret missing or invalid.")

        dhcp_config = {
            "Dhcp4": {
                "valid-lifetime": settings.DHCP_VALID_LIFETIME,
                "min-valid-lifetime": settings.DHCP_MIN_VALID_LIFETIME,
                "max-valid-lifetime": settings.DHCP_MAX_VALID_LIFETIME,
                "match-client-id": False,
                "calculate-tee-times": True,
                "control-socket": {
                    "socket-type": "unix",
                    "socket-name": settings.DHCP_CONTROL_SOCKET_PATH,
                },
                "interfaces-config": {
                    "interfaces": ["*"],
                    "dhcp-socket-type": "udp",
                    "outbound-interface": "use-routing",
                },
                "lease-database": {
                    "type": "postgresql",
                    "host": settings.DATABASES["kea"]["HOST"],
                    "port": settings.DATABASES["kea"]["PORT"],
                    "name": settings.DATABASES["kea"]["NAME"],
                    "user": settings.DATABASES["kea"]["USER"],
                    "password": settings.DATABASES["kea"]["PASSWORD"],
                },
                "option-def": [
                    {
                        "name": "no-pxedhcp",
                        "code": 176,
                        "type": "uint8",
                        "array": False,
                        "record-types": "",
                        "space": "ipxe",
                        "encapsulate": "",
                    },
                    {
                        "name": "https",
                        "code": 20,
                        "type": "uint8",
                        "array": False,
                        "record-types": "",
                        "space": "ipxe",
                        "encapsulate": "",
                    },
                ],
                "option-data": [
                    {
                        "name": "domain-name-servers",
                        "data": ", ".join(settings.DHCP_DOMAIN_NAME_SERVERS),
                        "always-send": True,
                    },
                    {
                        "name": "ntp-servers",
                        "data": ", ".join(settings.DHCP_NTP_SERVERS),
                        "always-send": True,
                    },
                    {
                        "name": "no-pxedhcp",
                        "code": 176,
                        "space": "ipxe",
                        "data": "1",
                        "always-send": True,
                    },
                ],
                "next-server": settings.DHCP_NEXT_SERVER,
                "client-classes": [
                    {
                        "name": "ipxe_efi_x86",
                        "test": "option[client-system].hex == 0x0006",
                        "boot-file-name": "ipxe.efi",
                    },
                    {
                        "name": "ipxe_efi_x64",
                        "test": "option[client-system].hex == 0x0007",
                        "boot-file-name": "ipxe.efi",
                    },
                    {
                        "name": "ipxe_efi_x64_x86",
                        "test": "option[client-system].hex == 0x0009",
                        "boot-file-name": "ipxe.efi",
                    },
                    {
                        "name": "ipxe_legacy_x64",
                        "test": "not option[client-system].hex == 0x0006 and not option[client-system].hex == 0x0007 and not option[client-system].hex == 0x0009",  # noqa: E501
                        "boot-file-name": "undionly.kpxe",
                    },
                ],
                "subnet4": list(
                    map(
                        lambda subnet: {
                            "id": subnet.id,
                            "subnet": str(subnet.subnet),
                            "pools": [
                                {
                                    "pool": f"{subnet.allocation_pool_start} - {subnet.allocation_pool_end}"  # noqa: E501
                                }
                            ],
                            "option-data": [
                                {
                                    "name": "domain-name",
                                    "data": ""
                                    if subnet.room_set.count() == 0
                                    else (
                                        subnet.room_set.first().domain
                                        if subnet.room_set.count() == 1
                                        else subnet.room_set.first().site.domain
                                    ),
                                },
                                {
                                    "name": "routers",
                                    "data": str(subnet.router),
                                },
                            ],
                            "reservations": [
                                {
                                    "hw-address": str(workstation.machine.mac),
                                    "ip-address": str(workstation.ip),
                                    "hostname": workstation.fqdn,
                                }
                                for workstation in Workstation.objects.filter(
                                    room__in=Room.objects.filter(dhcp_subnet=subnet)
                                ).select_related("machine")
                                if hasattr(workstation, "machine")
                            ],
                        },
                        DhcpSubnet.objects.all(),
                    )
                ),
                "loggers": [
                    {
                        "name": "kea-dhcp4",
                        "output_options": [
                            {
                                "output": "stdout",
                                "pattern": "%-5p %m\n",
                            },
                        ],
                        "severity": settings.DHCP_LOG_LEVEL,
                        "debuglevel": settings.DHCP_DEBUG_LEVEL,
                    },
                ],
            }
        }

        return JsonResponse(dhcp_config)


class PrometheusServiceDiscovery(View):
    http_method_names = ("get",)

    def get(self, request, *args, **kwargs):
        prom_sd_config = []

        for workstation in Workstation.objects.select_related(
            "room", "room__site"
        ).all():
            prom_sd_config.append(
                {
                    "targets": [f"{workstation.ip}:9100"],
                    "labels": {
                        "room": workstation.room.slug,
                        "site": workstation.room.site.domain,
                    },
                }
            )

        return JsonResponse(prom_sd_config, safe=False)


class Registered(TemplateView):
    template_name = "fleet/registered.ipxe"


class Register(TemplateView):
    form_class = RegisterForm
    template_name = "fleet/register.ipxe"
    success_url = reverse_lazy("fleet_registered")

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)

        try:
            room = Room.objects.get(currently_mapping=True)
        except Room.DoesNotExist:
            form.add_error(None, ValidationError("No room is currently mapping."))

        if not form.is_valid():
            return render(request, self.template_name, {"form": form})

        try:
            workstation = Workstation.objects.get(
                name=form.cleaned_data["name"], room=room
            )
            workstation.switch_name = form.cleaned_data["switch_name"]
            workstation.switch_port = form.cleaned_data["switch_port"]
        except Workstation.DoesNotExist:
            workstation = form.save(commit=False)
        workstation.room = room
        workstation.save()

        if hasattr(workstation, "machine"):
            old_machine = workstation.machine
            old_machine.workstation = None
            old_machine.save()

        Machine.objects.update_or_create(
            mac=form.cleaned_data["mac"],
            defaults={
                "workstation": workstation,
                "board_uuid": form.cleaned_data["board_uuid"],
                "board_serial": form.cleaned_data["board_serial"],
            },
        )

        return HttpResponseRedirect(self.success_url)


class SiteViewSet(ModelViewSet):
    queryset = Site.objects.all()
    serializer_class = SiteSerializer
    permission_classes = [DjangoModelWithViewPermissions]


class RoomViewSet(ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer
    permission_classes = [DjangoModelWithViewPermissions]
    filterset_fields = ("site",)


class WorkstationViewSet(ModelViewSet):
    queryset = Workstation.objects.all()
    serializer_class = WorkstationSerializer
    permission_classes = [DjangoModelWithViewPermissions]
    filterset_fields = ("room",)


class MachineViewSet(ModelViewSet):
    queryset = Machine.objects.all()
    serializer_class = MachineSerializer
    permission_classes = [DjangoModelWithViewPermissions]


class IssueViewSet(ModelViewSet):
    queryset = Issue.objects.all()
    serializer_class = IssueSerializer
    permission_classes = [DjangoModelPermissionsOrAnonReadOnly]
    filterset_class = IssueFilter

    @action(detail=True, methods=["patch"])
    def confirm(self, request, *args, **kwargs):
        self.get_object().confirm(request.user, save=True)
        return self.retrieve(request)

    @action(detail=True, methods=["patch"])
    def reject(self, request, *args, **kwargs):
        self.get_object().reject(request.user, save=True)
        return self.retrieve(request)

    @action(detail=True, methods=["patch"])
    def resolve(self, request, *args, **kwargs):
        self.get_object().resolve(request.user, save=True)
        return self.retrieve(request)
