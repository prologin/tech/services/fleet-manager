import django_filters


class IssueFilter(django_filters.FilterSet):
    ip = django_filters.CharFilter(field_name="workstation__ip")

    class Meta:
        fields = ("ip", "status")
