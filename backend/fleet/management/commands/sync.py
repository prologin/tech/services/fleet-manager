from django.apps import apps
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Synchronize all models"

    def print_sync_results(self, results):
        for adapter, (added, updated, deleted) in results.items():
            print(f"  {adapter.__name__}")
            errors = (added[1], updated[1], deleted[1])
            added_count = len(added[0])
            updated_count = len([o for o, u in updated[0].items() if u])
            deleted_count = len(deleted[0])
            error_count = sum(map(len, errors))
            for error_dict in errors:
                for obj_id, error in error_dict.items():
                    self.stderr.write(f"    {obj_id}: {error}")
            self.stdout.write(
                f"    added: {added_count}, updated: {updated_count}, "
                f"removed: {deleted_count}, errors: {error_count}"
            )

    def handle(self, *args, **options):
        for model in apps.get_models():
            if (
                hasattr(model, "sync_all")
                and not model._meta.proxy
                and not model._meta.abstract
            ):
                self.stdout.write(f"Syncing {model.__name__}...")
                self.print_sync_results(model.sync_all())
