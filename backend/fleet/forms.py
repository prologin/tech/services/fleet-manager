from django import forms

from .models import Workstation


class RegisterForm(forms.ModelForm):
    mac = forms.CharField(min_length=17, max_length=17)

    board_uuid = forms.UUIDField(required=False)
    board_serial = forms.CharField(max_length=255, required=False)

    class Meta:
        model = Workstation
        fields = (
            "name",
            "switch_name",
            "switch_port",
            "mac",
            "board_uuid",
            "board_serial",
        )

    # We check this manually in the Register view
    def validate_unique(self):
        pass
