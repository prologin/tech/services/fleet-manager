from django.db import models, transaction

from .backend import get_salt_backend

salt = get_salt_backend()


# See https://docs.saltproject.io/en/latest/ref/returners/all/salt.returners.pgjsonb.html # noqa: E501
# for schema reference.

class Jid(models.Model):
    jid = models.CharField(primary_key=True, db_index=True, max_length=255)
    load = models.JSONField()

    class Meta:
        db_table = "jids"

    def __str__(self):
        return self.jid


class Return(models.Model):
    _pk = models.BigAutoField(primary_key=True)
    fun = models.CharField(max_length=50, db_index=True)
    jid = models.CharField(max_length=255, db_index=True)
    return_field = models.JSONField(
        db_column="return"
    )  # renamed because `return` is python-reserved
    _id = models.CharField(max_length=255, db_index=True, db_column="id")
    _success = models.CharField(max_length=10, db_column="success")
    full_ret = models.JSONField()
    alter_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "salt_returns"
        ordering = ("-alter_time",)

    def __str__(self):
        return f"{self.jid} - {self.fun} - {self.success}"

    @property
    def args(self):
        if "fun_args" in self.full_ret and self.full_ret["fun_args"]:
            return [arg for arg in self.full_ret["fun_args"] if isinstance(arg, str)]
        return []

    @property
    def kwargs(self):
        if "fun_args" in self.full_ret and self.full_ret["fun_args"]:
            return [arg for arg in self.full_ret["fun_args"] if isinstance(arg, dict)]
        return []

    @property
    def success(self):
        return self._success == "true"


class Event(models.Model):
    id = models.BigAutoField(primary_key=True)
    tag = models.CharField(max_length=255, db_index=True)
    data = models.JSONField()
    alter_time = models.DateTimeField(auto_now_add=True)
    master_id = models.CharField(max_length=255)

    class Meta:
        db_table = "salt_events"

    def __str__(self):
        return self.tag


# From here on, these are our models


class Minion(models.Model):
    minion_id = models.CharField(max_length=128)
    grains = models.JSONField(default=dict)
    pillars = models.JSONField(default=dict)

    def __str__(self):
        return self.minion_id

    @property
    def last_job(self):
        return Return.objects.filter(_id=self.minion_id).order_by("-alter_time").first()

    @transaction.atomic
    def _set_minion_data(self, minion_data):
        for attr in ("grains", "pillars"):
            if minion_data[attr]:
                setattr(self, attr, minion_data[attr])

    @transaction.atomic
    def refresh(self, save=True):
        minion_data = salt.get_minion(self.minion_id)
        if not minion_data:
            return

        self._set_minion_data(minion_data)

        if save:
            self.save()

    @classmethod
    def populate(cls, minion_id):
        minion_data = salt.get_minion(minion_id)
        if not minion_data:
            return None
        with transaction.atomic():
            minion, _ = cls.objects.get_or_create(minion_id=minion_id)
            minion._set_minion_data(minion_data)
            minion.save()
        return minion

    @classmethod
    def _refresh_all(cls):
        minions = salt.get_minions()
        with transaction.atomic():
            for minion_id, minion_data in minions.items():
                minion, _ = cls.objects.get_or_create(minion_id=minion_id)
                minion._set_minion_data(  # # pylint:disable=protected-access
                    minion_data
                )
                minion.save()


class Key(models.Model):
    class Statuses(models.TextChoices):
        accepted = "minions", "Accepted"
        unaccepted = "minions_pre", "Unaccepted"
        denied = "minions_denied", "Denied"
        rejected = "minions_rejected", "Rejected"

    minion = models.ForeignKey(Minion, on_delete=models.CASCADE)
    status = models.CharField(max_length=64, choices=Statuses.choices)
    fingerprint = models.TextField(blank=True)

    def __str__(self):
        return f"{self.minion} - {self.get_status_display()} - {self.fingerprint}"

    @classmethod
    def _refresh_all(cls):
        keys = salt.get_keys()
        with transaction.atomic():
            cls.objects.all().delete()

            for status in cls.Statuses:
                for minion_id in keys[status]:
                    fingerprint = salt.get_key_fingerprint(minion_id)
                    if not fingerprint:
                        continue
                    minion, _ = Minion.objects.get_or_create(minion_id=minion_id)
                    cls.objects.create(
                        minion=minion, status=status, fingerprint=fingerprint
                    )
