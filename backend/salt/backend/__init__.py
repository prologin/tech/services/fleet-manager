from django.conf import settings


def get_salt_backend():
    from .pepper import SaltBackendPepper  # pylint:disable=import-outside-toplevel

    return SaltBackendPepper()


class SaltBackendException(Exception):
    pass


class SaltBackendBase:
    _instance = None

    key_statuses = (
        "minions",
        "minions_pre",
        "minions_denied",
        "minions_rejected",
    )

    def __new__(cls):
        """
        This is meant to implemented the Singleton pattern to avoid connecting
        too many times to our backend.
        """
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, config=settings.SALT_BACKEND_CONFIG):
        """
        Saves the config and connects to the backend if the backend set
        _connect_on_init to True. Some backends don't require connection,
        allowing us to skip those when relevant.
        """
        self._config = config
        if self._connect_on_init:
            self._get_connection()

    def _connect(self):
        """
        Connect to the backend. This may be creating a websocket, login, ...
        """
        raise NotImplementedError()

    @classmethod
    def _connection_expired(cls, connection=None):
        """
        Check if the connection is expired. In some cases, we have to log back
        in after a certain period of time.
        """
        raise NotImplementedError()

    def _get_connection(self):
        """
        Get the connection object for the backend. Checks if the connection
        has been made in __init__ or if it is expired, in which case it
        reconnects.
        """
        if not hasattr(self, "_connection"):
            self._connection = None
        if not self._connection or self._connection_expired(self._connection):
            self._connection = self._connect()
        return self._connection

    def _get_minion(self, minion_id):
        """
        Get information about a minion. Return format is:
        {
            "grains": {
                "grain": "value",
            },
            "pillars": {
                "pillar": "value",
            },
        }
        """
        raise NotImplementedError()

    def get_minion(self, minion_id=None):
        if not minion_id:
            raise SaltBackendException("minion_id was None.")
        minion = self._get_minion(minion_id)
        return minion if minion else None

    def _get_minions_list(self):
        """
        Get the list of currently connected minions. Return a list of minion
        IDs as strings.
        """
        raise NotImplementedError()

    def list_minions(self):
        return self._get_minions_list()

    def get_minions(self):
        """
        Get all currently connected minions. Return a dictionary with, as key,
        the minion ID, and as value, the return of get_minion.
        """
        minions = {}
        for minion_id in self.list_minions():
            minion = self._get_minion(minion_id)
            if minion:
                minions[minion_id] = minion
        return minions

    def _get_keys(self):
        raise NotImplementedError()

    def get_keys(self):
        """
        Get all keys sorted by status.
        """
        return self._get_keys()

    def _get_key_fingerprint(self, minion_id):
        raise NotImplementedError()

    def get_key_fingerprint(self, minion_id=None):
        """
        Get key fingerprint, matching on a minion_id.
        """
        if not minion_id:
            raise SaltBackendException("minion_id was None.")
        fingerprint = self._get_key_fingerprint(minion_id)
        return fingerprint if fingerprint else None

    def _run(self, selector, module, arg):
        raise NotImplementedError()

    # TODO: add support for kwargs
    def run(self, selector, module, arg=None):
        return self._run(selector, module, arg)

    def cmd_run(self, selector, command):
        return self.run(selector, "cmd.run", command)
