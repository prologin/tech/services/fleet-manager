from django.utils import timezone
from pepper import Pepper

from . import SaltBackendBase


class SaltBackendPepper(SaltBackendBase):
    _connect_on_init = False

    def _connect(self):
        pepper = Pepper(
            self._config["endpoint"],
            ignore_ssl_errors=self._config["ignore_ssl_errors"],
        )
        pepper.login(
            username="fleet",
            password=self._config["sharedsecret"],
            eauth="sharedsecret",
        )
        return pepper

    @classmethod
    def _connection_expired(cls, connection=None):
        return not (
            connection
            and "start" in connection.auth
            and "expire" in connection.auth
            and connection.auth["start"]
            <= timezone.now().timestamp() + 30
            < connection.auth["expire"]
        )

    def _get_minion(self, minion_id):
        pepper = self._get_connection()
        grains = pepper.local(minion_id, "grains.items")["return"][0].get(
            minion_id, False
        )
        pillars = pepper.local(minion_id, "pillar.items")["return"][0].get(
            minion_id, False
        )
        return {
            "grains": grains,
            "pillars": pillars,
        }

    def _get_minions_list(self):
        pepper = self._get_connection()
        ping = pepper.local("*", "test.ping")["return"][0]
        return [minion for minion in ping if ping[minion]]

    def _get_keys(self):
        pepper = self._get_connection()
        keys = pepper.wheel("key.list_all")["return"][0]["data"]["return"]
        if "local" in keys:
            keys.pop("local")
        return keys

    def _get_key_fingerprint(self, minion_id):
        pepper = self._get_connection()
        ret = pepper.wheel("key.finger", match=minion_id, hash_type="sha256")
        if not ret:
            return None
        fingerprint = ret["return"][0]["data"]["return"]
        for key in self.key_statuses:
            if key in fingerprint:
                return fingerprint[key][minion_id]
        return None

    def _run(self, selector, module, arg):
        pepper = self._get_connection()
        return pepper.local(selector, module, arg=arg)["return"][0]
