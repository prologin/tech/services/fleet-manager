from django.contrib import admin
from django.db import models
from jsoneditor.forms import JSONEditor

from .models import Event, Jid, Key, Minion, Return


@admin.register(Jid, Return, Event, Minion, Key)
class SaltAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.JSONField: {"widget": JSONEditor},
    }
