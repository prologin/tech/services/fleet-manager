from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet

from .tasks import minions_refresh_all
from .models import Minion
from .permissions import DjangoModelWithViewPermissions
from .serializers import MinionSerializer


class MinionViewSet(ModelViewSet):
    queryset = Minion.objects.all()
    serializer_class = MinionSerializer
    lookup_field = "minion_id"
    lookup_value_regex = "[^/]+"
    permission_classes = [DjangoModelWithViewPermissions]

    @action(detail=False, methods=["post"])
    def refresh_all(self, request):
        task = minions_refresh_all.delay()
        task.wait(timeout=2000)  # 2 seconds, maybe this should be a setting
        return self.list(request)

    @action(detail=True, methods=["post"])
    def refresh(self, request, *args, **kwargs):
        self.get_object().refresh()
        return self.retrieve(request, *args, **kwargs)
