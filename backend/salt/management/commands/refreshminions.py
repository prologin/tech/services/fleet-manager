from django.core.management.base import BaseCommand

from salt.models import Minion


class Command(BaseCommand):
    help = "Refresh all minions"

    def handle(self, *args, **options):
        Minion._refresh_all()
