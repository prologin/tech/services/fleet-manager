from celery import shared_task

from .models import Key, Minion


@shared_task
def minions_refresh_all():
    Minion._refresh_all()


@shared_task
def keys_refresh_all():
    Key._refresh_all()
