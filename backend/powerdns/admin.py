from django.contrib import admin

from .models import (
    Comment,
    CryptoKey,
    Domain,
    DomainMetadata,
    Record,
    Supermaster,
    TsigKey,
)


@admin.register(
    Comment, CryptoKey, DomainMetadata, Domain, Record, Supermaster, TsigKey
)
class PowerDnsAdmin(admin.ModelAdmin):
    pass
