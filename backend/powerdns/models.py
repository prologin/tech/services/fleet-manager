from django.db import models

# See PowerDNS documentation for Schema


class Domain(models.Model):
    name = models.CharField(unique=True, max_length=255)
    master = models.CharField(max_length=128, blank=True, null=True)
    last_check = models.IntegerField(blank=True, null=True)
    type = models.CharField(max_length=6)
    notified_serial = models.BigIntegerField(blank=True, null=True)
    account = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        db_table = "domains"

    def __str__(self):
        return self.name


class DomainMetadata(models.Model):
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE, blank=True, null=True)
    kind = models.CharField(max_length=32, blank=True, null=True)
    content = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "domainmetadata"


class Comment(models.Model):
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    type = models.CharField(max_length=10)
    modified_at = models.IntegerField()
    account = models.CharField(max_length=40, blank=True, null=True)
    comment = models.CharField(max_length=65535)

    class Meta:
        db_table = "comments"


class CryptoKey(models.Model):
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE, blank=True, null=True)
    flags = models.IntegerField()
    active = models.BooleanField(blank=True, null=True)
    published = models.BooleanField(blank=True, null=True)
    content = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "cryptokeys"


class Record(models.Model):
    id = models.BigAutoField(primary_key=True)
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=10, blank=True, null=True)
    content = models.CharField(max_length=65535, blank=True, null=True)
    ttl = models.IntegerField(blank=True, null=True)
    prio = models.IntegerField(blank=True, null=True)
    disabled = models.BooleanField(blank=True, null=True)
    ordername = models.CharField(max_length=255, blank=True, null=True)
    auth = models.BooleanField(blank=True, null=True)

    class Meta:
        db_table = "records"

    def __str__(self):
        return f"{self.name} {self.type} IN {self.ttl} {self.content}"


class Supermaster(models.Model):
    ip = models.GenericIPAddressField(primary_key=True)
    nameserver = models.CharField(max_length=255)
    account = models.CharField(max_length=40)

    class Meta:
        db_table = "supermasters"
        unique_together = (("ip", "nameserver"),)


class TsigKey(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    algorithm = models.CharField(max_length=50, blank=True, null=True)
    secret = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = "tsigkeys"
        unique_together = (("name", "algorithm"),)
