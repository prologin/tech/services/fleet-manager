from django.apps import AppConfig


class PowerDnsConfig(AppConfig):
    name = "powerdns"
