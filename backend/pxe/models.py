from ipaddress import ip_network

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse


class Image(models.Model):
    title = models.CharField(
        max_length=256,
        unique=True,
    )

    name = models.CharField(
        max_length=256,
        blank=True,
    )

    kernel = models.URLField()

    initrd = models.URLField(
        blank=True,
    )

    cmdline = models.CharField(
        max_length=256,
        blank=True,
    )

    class Meta:
        ordering = ("name",)

    def _format(self, value):
        return value.format(
            title=self.title,
            name=self.name,
        )

    def get_kernel(self):
        return self._format(self.kernel)

    def get_kernel_filename(self):
        return self.get_kernel().split("/")[-1]

    def get_initrd(self):
        return self._format(self.initrd)

    def get_initrd_filename(self):
        return self.get_initrd().split("/")[-1]

    def get_cmdline(self, extra=""):
        return self._format(
            " ".join(
                filter(None, [self.cmdline, extra, settings.CMDLINE_OVERRIDE or ""])
            )
        )

    def __str__(self):
        return self.title


class ItemCategory(models.Model):
    title = models.CharField(
        max_length=256,
        unique=True,
    )

    label = models.CharField(
        max_length=80,
        blank=True,
    )

    priority = models.SmallIntegerField(
        default=0,
        blank=True,
    )

    @staticmethod
    def categorize_items(items):
        items_with_category = Item.objects.filter(
            id__in=[i.id for i in items],
        ).exclude(
            category__isnull=True,
        )
        items_without_category = Item.objects.filter(
            id__in=[i.id for i in items],
        ).exclude(
            category__isnull=False,
        )
        category = None
        separator = Item(
            label="",
            type="separator",
        )
        if not items_with_category:
            yield from items_without_category
        for item in items_with_category:
            if category != item.category:
                if category is not None:
                    yield separator
                category = item.category
                if items_without_category and category.priority >= 0:
                    yield from items_without_category
                    yield separator
                    items_without_category = []
                if category and category.label:
                    yield category.to_separator()
            yield item

    class Meta:
        ordering = ("priority", "label")
        verbose_name_plural = "item categories"

    def get_items(self):
        return self.item_set.all()

    def to_separator(self):
        caret_count = (78 - len(self.label)) // 2
        label = "{carets} {label} {carets}".format(
            carets="-" * caret_count,
            label=self.label,
        )
        if len(label) < 80:
            label += "-"
        return Item(
            category=self,
            priority=-32768,
            label=label,
            type="separator",
        )

    def __str__(self):
        return self.title


class Item(models.Model):
    TYPE_CHOICES = (
        ("submenu", "Submenu"),
        ("img", "Image"),
        ("uri", "URI"),
        ("custom", "Custom script"),
        ("separator", "Separator"),
    )

    category = models.ForeignKey(
        ItemCategory,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    priority = models.SmallIntegerField(
        default=0,
        blank=True,
    )

    label = models.CharField(max_length=80)

    type = models.CharField(
        max_length=10,
        choices=TYPE_CHOICES,
    )

    key = models.CharField(
        max_length=4,
        blank=True,
    )

    # type == 'submenu'

    submenu = models.ForeignKey(
        "Menu",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    # type == 'img'

    image = models.ForeignKey(
        Image,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )

    extra_cmdline = models.CharField(
        max_length=256,
        blank=True,
    )

    # type == 'uri'

    uri = models.URLField(
        blank=True,
    )

    # type == 'custom'

    script = models.TextField(
        blank=True,
    )

    class Meta:
        ordering = ("category__priority", "category__label", "priority", "label")

    def __str__(self):
        if not self.category:
            return f"{self.type} - {self.label}"
        label = f"[{self.category.label}]" if self.category.label else ""
        return f"{label}{self.type} - {self.label}"

    def get_cmdline(self):
        return self.image.get_cmdline(extra=self.extra_cmdline)

    def get_printable_key_ipxe(self):
        if not self.key:
            return " ${}" * 4

        if self.key[0:2] in ("0x", "0X"):
            value = int(self.key, 16)
        else:
            return self.key[0] + " ${}" * 2

        if value == 0x1B:
            return "ESC"

        if 0x01 <= value <= 0x1A:
            return f"C-{chr(ord('a') + value - 1)}"

        return " ${}" * 4

    def get_printable_key(self):
        if not self.key:
            return ""

        if self.key[0:2] in ("0x", "0X"):
            value = int(self.key, 16)
        else:
            return self.key[0]

        if value == 0x1B:
            return "ESC"

        if 0x01 <= value <= 0x1A:
            return f"C-{chr(ord('a') + value - 1)}"

        return ""

    def get_label(self):
        if self.type == "submenu":
            return f"{self.label}..."
        return self.label


class ItemGroup(models.Model):
    title = models.CharField(
        max_length=80,
        blank=True,
    )

    items = models.ManyToManyField(
        Item,
        blank=True,
    )

    categories = models.ManyToManyField(
        ItemCategory,
        blank=True,
    )

    def get_items(self):
        items = set(self.items.all())
        for category in self.categories.all():
            items.update(category.get_items())
        return Item.objects.filter(id__in=[i.id for i in items])

    def __str__(self):
        return self.title


class Menu(models.Model):
    title = models.CharField(max_length=80)

    item_groups = models.ManyToManyField(ItemGroup, blank=True)

    items = models.ManyToManyField(
        Item,
        blank=True,
    )

    default_item = models.ForeignKey(
        Item,
        blank=True,
        null=True,
        related_name="menu_default",
        on_delete=models.SET_NULL,
    )

    timeout = models.PositiveIntegerField(blank=True, null=True, help_text="in second")

    class Meta:
        ordering = ("title",)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse(
            "htmlmenu",
            kwargs={
                "pk": self.pk,
            },
        )

    def clean(self):
        if self.timeout is not None and not self.default_item:
            raise ValidationError(
                {"default_item": "Field 'timeout' is set but 'default_item' is not!"}
            )

    def get_ipxe_timeout(self):
        if self.timeout is None:
            return None
        return self.timeout * 1000

    def get_items(self):
        items = set(self.items.all())
        for group in self.item_groups.all():
            items.update(group.get_items())
        if self.default_item is not None:
            items.add(self.default_item)
        return ItemCategory.categorize_items(items)


class MatchingGroup(models.Model):
    name = models.CharField(
        max_length=256,
        unique=True,
    )

    menu = models.ForeignKey(
        Menu,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    def __str__(self):
        return self.name


class Matcher(models.Model):
    group = models.ForeignKey(
        MatchingGroup,
        on_delete=models.CASCADE,
    )

    class Meta:
        abstract = True

    @classmethod
    def match(cls, **kwargs):
        groups = set()
        for model in (MacAddressMatcher, IPAddressMatcher, NetworkMatcher):
            groups |= set(m.group for m in model.match(**kwargs))
        return groups


class MacAddressMatcher(Matcher):
    mac_address = models.CharField(
        max_length=256,
    )

    def __str__(self):
        return self.mac_address

    # pylint:disable=arguments-differ
    @classmethod
    def match(cls, mac_address=None, **_kwargs):
        if mac_address is None:
            return cls.objects.none()
        return cls.objects.filter(mac_address=mac_address)


class IPAddressMatcher(Matcher):
    ip_address = models.GenericIPAddressField(
        unpack_ipv4=True,
    )

    class Meta:
        verbose_name = "IP address matcher"

    def __str__(self):
        return self.ip_address

    # pylint:disable=arguments-differ
    @classmethod
    def match(cls, ip_address=None, **_kwargs):
        if ip_address is None:
            return cls.objects.none()
        return cls.objects.filter(ip_address=str(ip_address))


class NetworkMatcher(Matcher):
    network_address = models.GenericIPAddressField(
        unpack_ipv4=True,
    )

    prefix_length = models.PositiveSmallIntegerField()

    def to_python(self):
        return ip_network((self.network_address, self.prefix_length))

    def clean(self):
        try:
            self.to_python()
        except ValueError as e:
            raise ValidationError("Invalid network specified") from e

    def __str__(self):
        return str(self.to_python())

    # pylint:disable=arguments-differ
    @classmethod
    def match(cls, ip_address=None, **_kwargs):
        if ip_address is None:
            return cls.objects.none()
        result = []
        prefixlen = -1
        for nm in cls.objects.all():
            if ip_address in nm.to_python():
                if nm.prefix_length == prefixlen:
                    result.append(nm)
                if nm.prefix_length > prefixlen:
                    result = [nm]
                    prefixlen = nm.prefix_length
        return result
