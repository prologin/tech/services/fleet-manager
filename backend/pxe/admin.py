from copy import deepcopy

from admin_ordering.admin import OrderableAdmin
from django.conf import settings
from django.contrib import admin

from . import models


@admin.register(models.Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ("title", "name", "kernel", "initrd", "cmdline")
    list_display_links = list_display
    list_filter = ("name",)
    search_fields = ("name", "kernel", "initrd", "cmdline")
    prepopulated_fields = {
        "name": ("title",),
    }
    default_values = {
        "kernel": settings.DEFAULT_KERNEL_VALUE,
        "initrd": settings.DEFAULT_INITRD_VALUE,
        "cmdline": settings.DEFAULT_CMDLINE_VALUE,
    }

    def get_changeform_initial_data(self, request):
        r = super().get_changeform_initial_data(request)
        r.update(self.default_values)
        return r


class ItemAdminInline(OrderableAdmin, admin.TabularInline):
    model = models.Item
    fields = ("priority", "label", "type")
    readonly_fields = ("label", "type")

    fk_name = "category"
    ordering_field = "priority"
    ordering_field_hide_input = True

    def has_add_permission(self, request, obj):
        return False


@admin.register(models.ItemCategory)
class ItemCategoryAdmin(OrderableAdmin, admin.ModelAdmin):
    list_display = ("priority", "title", "label")
    list_display_links = (
        "title",
        "label",
    )
    list_editable = ("priority",)
    search_fields = ("title", "label")
    inlines = (ItemAdminInline,)

    fields = ("title", "label")

    ordering_field = "priority"
    ordering_field_hide_input = True


@admin.register(models.Item)
class ItemAdmin(OrderableAdmin, admin.ModelAdmin):
    list_display = ("key", "label", "category", "priority", "type")
    list_display_links = ("category", "key", "label", "type")
    list_editable = ("priority",)
    list_filter = ("category", "type", "image", "key")
    search_fields = (
        "category__label",
        "label",
        "type",
        "submenu__title",
        "uri",
        "script",
        "image__name",
        "extra_cmdline",
    )
    autocomplete_fields = (
        "category",
        "submenu",
    )
    radio_fields = {
        "type": admin.HORIZONTAL,
    }
    fieldsets = (
        (
            None,
            {
                "fields": ("label", "type", "key"),
            },
        ),
        (
            "Category",
            {
                "fields": ("category",),
            },
        ),
        (
            "Submenu / Inline menu",
            {
                "for_type": ("submenu", "inline"),
                "fields": ("submenu",),
            },
        ),
        (
            "Image",
            {
                "for_type": ("img",),
                "fields": ("image", "extra_cmdline"),
            },
        ),
        (
            "URI",
            {
                "for_type": ("uri",),
                "fields": ("uri",),
            },
        ),
        (
            "Custom script",
            {
                "for_type": ("custom",),
                "fields": ("script",),
            },
        ),
    )

    ordering_field = "priority"
    ordering_field_hide_input = True

    # pylint:disable=signature-differs
    def get_fieldsets(self, request, obj):
        fieldsets = deepcopy(self.fieldsets)
        for _, block in fieldsets:
            if obj:
                if "for_type" in block and obj.type not in block["for_type"]:
                    block.setdefault("classes", []).append("collapse")
            block["for_type"] = None
            del block["for_type"]
        return fieldsets


@admin.register(models.ItemGroup)
class ItemGroupAdmin(admin.ModelAdmin):
    list_display = ("title",)
    list_display_links = list_display
    search_fields = ("title",)
    filter_horizontal = ("items", "categories")


@admin.register(models.Menu)
class MenuAdmin(admin.ModelAdmin):
    list_display = ("title", "timeout", "default_item")
    list_display_links = ("title",)
    list_editable = ("timeout", "default_item")
    search_fields = ("title",)
    filter_horizontal = ("item_groups", "items")
    autocomplete_fields = ("default_item",)


class MacAddressMatcherAdminInline(admin.TabularInline):
    model = models.MacAddressMatcher
    extra = 1


class IPAddressMatcherAdminInline(admin.TabularInline):
    model = models.IPAddressMatcher
    extra = 1


class NetworkMatcherAdminInline(admin.TabularInline):
    model = models.NetworkMatcher
    extra = 1


@admin.register(models.MatchingGroup)
class MatchingGroupAdmin(admin.ModelAdmin):
    search_fields = ("name",)
    autocomplete_fields = ("menu",)
    inlines = (
        MacAddressMatcherAdminInline,
        IPAddressMatcherAdminInline,
        NetworkMatcherAdminInline,
    )
