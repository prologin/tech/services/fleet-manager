# noqa
# pylint: skip-file

from pathlib import Path

from django_utils.settings.caches import *
from django_utils.settings.common import *
from django_utils.settings.databases import *
from django_utils.settings.logging import *

from django_utils.settings.auth import *  # isort:skip
from django_utils.settings.celery import *  # isort:skip
from django_utils.settings.drf import *  # isort:skip


PROJECT_NAME = "fleet_manager"

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent

INSTALLED_APPS = installed_apps(with_auth=True) + [
    "admin_ordering",
    "django_filters",
    "jsoneditor",
    "netfields",
    "fleet.apps.FleetConfig",
    "kea.apps.KeaConfig",
    "powerdns.apps.PowerDnsConfig",
    "pxe.apps.PXEConfig",
    "salt.apps.SaltConfig",
    "sessions_state.apps.SessionsStateConfig",
]

MIDDLEWARE = middleware(with_auth=True)

DATABASES = databases(PROJECT_NAME)
DATABASES["kea"] = {
    **DATABASES["default"],
    "NAME": env.get_string("DB_KEA_NAME", "kea"),
    "USER": env.get_string("DB_KEA_USER", "kea"),
    "PASSWORD": env.get_secret("DB_KEA_PASSWORD"),
    "HOST": env.get_string("DB_KEA_HOST"),
    "PORT": env.get_int("DB_KEA_PORT", 5432),
}
DATABASES["powerdns"] = {
    **DATABASES["default"],
    "NAME": env.get_string("DB_POWERDNS_NAME", "powerdns"),
    "USER": env.get_string("DB_POWERDNS_USER", "powerdns"),
    "PASSWORD": env.get_secret("DB_POWERDNS_PASSWORD"),
    "HOST": env.get_string("DB_POWERDNS_HOST"),
    "PORT": env.get_int("DB_POWERDNS_PORT", 5432),
}
DATABASES["salt"] = {
    **DATABASES["default"],
    "NAME": env.get_string("DB_SALT_NAME", "salt"),
    "USER": env.get_string("DB_SALT_USER", "salt"),
    "PASSWORD": env.get_secret("DB_SALT_PASSWORD"),
    "HOST": env.get_string("DB_SALT_HOST"),
    "PORT": env.get_int("DB_SALT_PORT", 5432),
}

DATABASE_ROUTERS = ("fleet_manager.database_routers.AppRouter",)

ROOT_URLCONF = root_urlconf(PROJECT_NAME)

WSGI_APPLICATION = wsgi_application(PROJECT_NAME)

REST_FRAMEWORK = rest_framework(with_auth=True)
REST_FRAMEWORK["DEFAULT_FILTER_BACKENDS"] = ("fleet_manager.drf.DjangoFilterBackend",)
REST_FRAMEWORK["DEFAULT_PAGINATION_CLASS"] = "rest_framework.pagination.LimitOffsetPagination"
REST_FRAMEWORK["PAGE_SIZE"] = 10000


# PXE specific settings

DEFAULT_KERNEL_VALUE = "https://images.pie.prologin.org/{name}_bzImage"
DEFAULT_INITRD_VALUE = "https://images.pie.prologin.org/{name}_initrd"
DEFAULT_CMDLINE_VALUE = "vga=0x31a loglevel=4 initrd={name}_initrd"
CMDLINE_OVERRIDE = "net.ifnames=0"

# Sessions state specific settings
SESSIONS_MERGE_TIMEOUT_SECONDS = env.get_int("SESSIONS_MERGE_TIMEOUT_SECONDS", 3600)

SESSIONS_IGNORE_USERS = env.get_list("SESSIONS_IGNORE_USERS", ("sddm", "lightdm"))

SESSIONS_IGNORE_SUBNETS = env.get_list("SESSIONS_IGNORE_SUBNETS", ("127.0.0.0/8",))


# Kea specific settings

KEA_SHARED_SECRET = env.get_secret("KEA_SHARED_SECRET")
# These are used to generate Kea's config
DHCP_VALID_LIFETIME = env.get_int("DHCP_VALID_LIFETIME", 3600)
DHCP_MIN_VALID_LIFETIME = env.get_int("DHCP_MIN_VALID_LIFETIME", 360)
DHCP_MAX_VALID_LIFETIME = env.get_int("DHCP_MAX_VALID_LIFETIME", 7200)
DHCP_CONTROL_SOCKET_PATH = env.get_string(
    "DHCP_CONTROL_SOCKET_PATH", "/shared/kea4-ctrl-socket"
)
DHCP_LOG_LEVEL = env.get_string("DHCP_LOG_LEVEL", "INFO")
DHCP_DEBUG_LEVEL = env.get_int("DHCP_DEBUG_LEVEL", 0)

DHCP_DOMAIN_NAME_SERVERS = env.get_list(
    "DHCP_DOMAIN_NAME_SERVERS", ["1.1.1.1", "1.0.0.1"]
)
DHCP_NTP_SERVERS = env.get_list("DHCP_NTP_SERVERS", [])
DHCP_NEXT_SERVER = env.get_string("DHCP_NEXT_SERVER")


# DNS specific settings

DNS_SOA_MNAME = env.get_string("DNS_SOA_MNAME", "a.misconfigured.dns.server.invalid")
DNS_SOA_RNAME = env.get_string("DNS_SOA_RNAME", None)
DNS_SOA_REFRESH = env.get_int("DNS_SOA_REFRESH", 14400)
DNS_SOA_RETRY = env.get_int("DNS_SOA_RETRY", 3600)
DNS_SOA_EXPIRE = env.get_int("DNS_SOA_EXPIRE", 604800)
DNS_SOA_TTL = env.get_int("DNS_SOA_TTL", 300)

DNS_NS_NAME = env.get_string("DNS_NS_NAME", "a.misconfigured.dns.server.invalid")

DNS_RECORD_DEFAULT_TTL = env.get_int("DNS_RECORD_DEFAULT_TTL", DNS_SOA_TTL)


# Salt specific settings

SALT_BACKEND_CONFIG = {
    "endpoint": env.get_string("SALT_API_ENDPOINT"),
    "sharedsecret": env.get_secret("SALT_API_SHARED_SECRET"),
    "ignore_ssl_errors": env.get_bool("SALT_API_IGNORE_SSL_ERRORS", True),
}
