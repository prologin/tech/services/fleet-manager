# noqa
# pylint: skip-file
from django_utils.celery.app import *

app.conf.beat_schedule["salt-refresh-minions"] = {
   "task": "salt.tasks.minions_refresh_all",
   "schedule": 60.0 * 5, # every 5 minutes
}

app.conf.beat_schedule["salt-refresh-keys"] = {
   "task": "salt.tasks.keys_refresh_all",
   "schedule": 60.0 * 60, # every hour
}

app.conf.beat_schedule["sessions-merge"] = {
   "task": "sessions_state.tasks.sessions_merge",
   "schedule": 60.0 * 60,  # every hour
}

app.conf.beat_schedule["sessions-discover"] = {
   "task": "sessions_state.tasks.sessions_discover",
   "schedule": 60.0,  # every minute
}
