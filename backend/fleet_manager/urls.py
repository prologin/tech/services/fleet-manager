from django.urls import include, path
from django_utils.urls import default, drf

apps_with_api = ("fleet", "salt", "sessions_state")

urlpatterns = (
    default.urlpatterns()
    + drf.urlpatterns(apps_with_api, with_auth=True)
    + [
        path("pxe/", include("pxe.urls")),
    ]
)
