class AppRouter:
    route_app_labels = ("kea", "powerdns", "salt")

    # pylint:disable=unused-argument
    def db_for_read(self, model, **hints):
        if model._meta.app_label in self.route_app_labels:
            return model._meta.app_label
        return "default"

    # pylint:disable=unused-argument
    def db_for_write(self, model, **hints):
        if model._meta.app_label in self.route_app_labels:
            return model._meta.app_label
        return "default"

    # pylint:disable=unused-argument
    def allow_relation(self, obj1, obj2, **hints):
        if (
            obj1._meta.app_label in self.route_app_labels
            or obj2._meta.app_label in self.route_app_labels
        ) and obj1._meta.app_label != obj2._meta.app_label:
            return False
        return True

    # pylint:disable=unused-argument
    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if db in self.route_app_labels:
            return app_label == db
        return app_label not in self.route_app_labels
