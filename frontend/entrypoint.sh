#! /usr/bin/env bash

set -eo pipefail

if [ "$#" -eq 0 ]; then
  if [ -n "$DEV" ]; then
    set -- yarn dev
  else
    set -- yarn start
  fi
fi

exec "$@"
