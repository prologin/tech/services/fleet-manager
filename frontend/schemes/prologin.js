import qs from "querystring"
import axios from "axios"
import bodyParser from "body-parser"
import defu from "defu"

// From https://github.com/nuxt-community/auth-module/blob/dfbbb540c5e6c1f0bff0b356c46f50d48ee3f9a5/src/utils/provider.ts
function assignDefaults(strategy, defaults) {
  Object.assign(strategy, defu(strategy, defaults))
}

// From https://github.com/nuxt-community/auth-module/blob/dfbbb540c5e6c1f0bff0b356c46f50d48ee3f9a5/src/utils/provider.ts
function addAuthorize(nuxt, strategy, useForms = false) {
  // Get clientSecret, clientId, endpoints.token and audience
  const clientSecret = strategy.clientSecret
  const clientID = strategy.clientId
  const tokenEndpoint = strategy.endpoints.token
  const audience = strategy.audience

  // IMPORTANT: remove clientSecret from generated bundle
  delete strategy.clientSecret

  // Endpoint
  const endpoint = `/_auth/oauth/${strategy.name}/authorize`
  strategy.endpoints.token = endpoint

  // Set response_type to code
  strategy.responseType = "code"

  // Form data parser
  const formMiddleware = bodyParser.urlencoded({ extended: true })

  // Register endpoint
  nuxt.options.serverMiddleware.unshift({
    path: endpoint,
    handler: (req, res, next) => {
      if (req.method !== "POST") {
        return next()
      }

      formMiddleware(req, res, () => {
        const {
          code,
          code_verifier: codeVerifier,
          redirect_uri: redirectUri = strategy.redirectUri,
          response_type: responseType = strategy.responseType,
          grant_type: grantType = strategy.grantType,
          refresh_token: refreshToken
        } = req.body

        // Grant type is authorization code, but code is not available
        if (grantType === "authorization_code" && !code) {
          return next()
        }

        // Grant type is refresh token, but refresh token is not available
        if (grantType === "refresh_token" && !refreshToken) {
          return next()
        }

        let data = {
          client_id: clientID,
          client_secret: clientSecret,
          refresh_token: refreshToken,
          grant_type: grantType,
          response_type: responseType,
          redirect_uri: redirectUri,
          audience,
          code_verifier: codeVerifier,
          code
        }

        console.log(data)

        const headers = {
          Accept: "application/json",
          "Content-Type": "application/json"
        }

        if (useForms) {
          data = qs.stringify(data)
          headers["Content-Type"] = "application/x-www-form-urlencoded"
        }

        axios
          .request({
            method: "post",
            url: tokenEndpoint,
            data,
            headers
          })
          .then(response => {
            res.end(JSON.stringify(response.data))
          })
          .catch(error => {
            console.log(error)
            console.log(error.response.data)
            res.statusCode = error.response.status
            res.end(JSON.stringify(error.response.data))
          })
      })
    }
  })
}

export default function(nuxt, strategy) {
  const DEFAULTS = {
    scheme: "openIDConnect",
    acrValues: "goauthentik.io/providers/oauth2/default",
    grantType: "authorization_code",
    codeChallengeMethod: "S256",
    scope: ["openid", "profile", "email", "roles"]
  }

  assignDefaults(strategy, DEFAULTS)

  addAuthorize(nuxt, strategy, true)
}
