import Vue from 'vue'

export const state = () => ({
  sessionStates: {},
  sessions: {},
  sessionStats: {},
})

export const actions = {
  async getSessionsStates({ commit }, login) {
    const url = `/sessions_state/session-states/?login=${login}`
    const response = await this.$axios.get(url)
    const sessionStates = response.data.results

    sessionStates.forEach((sessionState) =>
      commit('sessionState', sessionState)
    )
  },
  async getSessions({ commit }, login) {
    const url = `/sessions_state/sessions/?login=${login}`
    const response = await this.$axios.get(url)
    const sessions = response.data.results

    sessions.forEach((session) => commit('session', session))
  },
  async getSessionStatesByRoomCurrentLatest({ commit }, roomId) {
    const url = `/sessions_state/session-states/?room=${roomId}&current=true&latest=true`
    // const url = `/sessions_state/session-states/?room=${roomId}&latest=true`
    const response = await this.$axios.get(url)
    const sessionStates = response.data.results

    sessionStates.forEach((sessionState) =>
      commit('sessionState', sessionState)
    )
  },
  async getSessionStatsByRoom({ commit }, roomId) {
    const url = `/sessions_state/sessions/stats/?room=${roomId}`
    const response = await this.$axios.get(url)
    const sessionStats = response.data

    sessionStats.forEach((sessionStat) => commit('sessionStat', sessionStat))
  },
}

export const mutations = {
  sessionState(state, sessionState) {
    if (sessionState.timestamp !== null) {
      sessionState.timestamp = new Date(sessionState.timestamp)
    }

    Vue.set(state.sessionStates, sessionState.id, sessionState)
  },

  clearSessionStates(state) {
    Vue.set(state, 'sessionStates', {})
  },

  session(state, session) {
    if (session.interval !== null) {
      session.interval.lower = new Date(session.interval.lower)
      session.interval.upper = new Date(session.interval.upper)
    }

    Vue.set(state.sessions, session.id, session)
  },

  sessionStat(state, sessionStat) {
    Vue.set(state.sessionStats, sessionStat.ip, sessionStat.duration)
  },
}

export const getters = {
  sessionStatesByLogin: (state) => {
    const sessionStates = {}
    Object.values(state.sessionStates).forEach((sessionState) => {
      if (!(sessionState.login in sessionStates)) {
        sessionStates[sessionState.login] = []
      }
      sessionStates[sessionState.login].push(sessionState)
    })
    return sessionStates
  },
  sessionsByLogin: (state) => {
    const sessions = {}
    Object.values(state.sessions).forEach((session) => {
      if (!(session.login in sessions)) {
        sessions[session.login] = []
      }
      sessions[session.login].push(session)
    })
    return sessions
  },
  sessionStatesByIP: (state) => {
    const sessionStates = {}
    Object.values(state.sessionStates).forEach((sessionState) => {
      if (!(sessionState.ip in sessionStates)) {
        sessionStates[sessionState.ip] = []
      }
      sessionStates[sessionState.ip].push(sessionState)
    })
    return sessionStates
  },
  sessionStats: (state) => {
    return state.sessionStats
  },
}
