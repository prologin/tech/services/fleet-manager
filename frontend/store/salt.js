import Vue from 'vue'

export const state = () => ({
  minions: {},
})

export const actions = {
  async getMinions({ commit }) {
    const url = '/salt/minions/'
    const response = await this.$axios.get(url)
    const minions = response.data.results

    minions.forEach((minion) => commit('minion', minion))
  },
  async refreshMinions({ commit }) {
    const url = '/salt/minions/refresh_all/'
    const response = await this.$axios.post(url)
    const minions = response.data.results

    minions.forEach((minion) => commit('minion', minion))
  },
  async getMinion({ commit }, minionId) {
    const url = `/salt/minions/${minionId}`
    const response = await this.$axios.get(url)
    const minion = response.data

    commit('minion', minion)
  },
  async refreshMinion({ commit }, minionId) {
    const url = `/salt/minions/${minionId}/refresh/`
    const response = await this.$axios.post(url)
    const minion = response.data

    commit('minion', minion)
  },
}

export const mutations = {
  minion(state, minion) {
    if (minion.last_job !== null) {
      minion.last_job = new Date(minion.last_job)
    }
    Vue.set(state.minions, minion.minion_id, minion)
  },
}

export const getters = {
  minions: (state) => {
    return state.minions
  },
  minion: (state) => (minionId) => {
    return state.minions[minionId]
  },
}
