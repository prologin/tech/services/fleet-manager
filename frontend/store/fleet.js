import Vue from 'vue'

export const state = () => ({
  sites: {},
  rooms: {},
  workstations: {},
})

export const actions = {
  async getSites({ commit }) {
    const url = '/fleet/sites/'
    const response = await this.$axios.get(url)
    const sites = response.data.results

    sites.forEach((site) => commit('site', site))
  },
  async getSite({ commit }, siteId) {
    const url = `/fleet/sites/${siteId}/`
    const response = await this.$axios.get(url)
    const site = response.data

    commit('site', site)
  },
  async getRooms({ commit }) {
    const url = `/fleet/rooms/`
    const response = await this.$axios.get(url)
    const rooms = response.data.results

    rooms.forEach((room) => commit('room', room))
  },
  async getRoomsBySite({ commit }, siteId) {
    const url = `/fleet/rooms/?site=${siteId}`
    const response = await this.$axios.get(url)
    const rooms = response.data.results

    rooms.forEach((room) => commit('room', room))
  },
  async getRoom({ commit }, roomId) {
    const url = `/fleet/rooms/${roomId}/`
    const response = await this.$axios.get(url)
    const room = response.data

    commit('room', room)
  },
  async getWorkstationsByRoom({ commit }, roomId) {
    const url = `/fleet/workstations/?room=${roomId}`
    const response = await this.$axios.get(url)
    const workstations = response.data.results

    workstations.forEach((workstation) => commit('workstation', workstation))
  },
  async getWorkstation({ commit }, workstationId) {
    const url = `/fleet/workstations/${workstationId}/`
    const response = await this.$axios.get(url)
    const workstation = response.data

    commit('workstation', workstation)
  },
  async submitIssue({ commit }, data) {
    const url = '/fleet/issues/'
    const payload = {
      workstation: data.workstationUrl,
      description: data.description,
      problem_type: data.problemType,
      severity: data.severity,
    }
    await this.$axios.post(url, payload)
  },
  async confirmIssue({ commit }, issueId) {
    const url = `/fleet/issues/${issueId}/confirm/`
    await this.$axios.patch(url)
  },
  async rejectIssue({ commit }, issueId) {
    const url = `/fleet/issues/${issueId}/reject/`
    await this.$axios.patch(url)
  },
  async resolveIssue({ commit }, issueId) {
    const url = `/fleet/issues/${issueId}/resolve/`
    await this.$axios.patch(url)
  },
}

export const mutations = {
  site(state, site) {
    Vue.set(state.sites, site.id, site)
  },
  room(state, room) {
    Vue.set(state.rooms, room.id, room)
  },
  workstation(state, workstation) {
    Vue.set(state.workstations, workstation.id, workstation)
  },
}

export const getters = {
  sites: (state) => {
    return state.sites
  },
  site: (state) => (siteId) => {
    return state.sites[siteId]
  },
  roomsBySite: (state) => {
    const rooms = {}
    Object.values(state.rooms).forEach((room) => {
      if (!(room.site in rooms)) {
        rooms[room.site] = []
      }
      rooms[room.site].push(room)
    })
    return rooms
  },
  room: (state) => (roomId) => {
    return state.rooms[roomId]
  },
  workstationsByRoom: (state) => {
    const workstations = {}
    Object.values(state.workstations).forEach((workstation) => {
      if (!(workstation.room in workstations)) {
        workstations[workstation.room] = []
      }
      workstations[workstation.room].push(workstation)
    })
    return workstations
  },
}
