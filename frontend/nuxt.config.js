export default {
  head: {
    titleTemplate: '%s - Fleet Manager',
    title: 'Fleet Manager',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/logo.png' }],
  },

  css: ['~/assets/css/main.css'],

  plugins: [],

  components: true,

  buildModules: [
    // '@nuxtjs/eslint-module',
    // '@nuxtjs/stylelint-module',
  ],

  build: {
    transpile: ['defu'],
  },

  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
  ],

  axios: {
    baseURL: process.env.BACKEND_URL === undefined ? 'http://backend_dev:8000/rest' : process.env.BACKEND_URL,
    browserBaseURL: process.env.BROWSER_BACKEND_URL === undefined ? 'http://localhost:8000/rest' : process.env.BROWSER_BACKEND_URL,
  },

  auth: {
    strategies: {
      prologin: {
        provider: "~/schemes/prologin",
        clientId: process.env.OIDC_RP_CLIENT_ID,
        clientSecret: process.env.OIDC_RP_CLIENT_SECRET,
        endpoints: {
          configuration: process.env.OIDC_OP_CONFIG_URL + '/.well-known/openid-configuration',
          token: process.env.OIDC_OP_TOKEN_URL,
        },
      },
    },
    redirect: {
      login: '/login',
      callback: "/api/auth/callback/prologin",
      home: "/",
    },
  },

  bootstrapVue: {
    icons: true,
    config: {
      BCard: {
        bgVariant: 'dark',
        textVariant: 'white',
      },
      BModal: {
        bodyBgVariant: 'dark',
        bodyTextVariant: 'light',
        headerBgVariant: 'dark',
        headerBorderVariant: 'dark',
        headerTextVariant: 'light',
        footerBgVariant: 'dark',
        footerBorderVariant: 'dark',
        footerTextVariant: 'light',
        titleVariant: 'dark',
      },
      BNavbar: {
        type: 'dark',
        variant: 'dark',
      },
      BTable: {
        footVariant: 'dark',
        headVariant: 'dark',
        hover: true,
        noBorderCollapse: true,
        stickyHeader: true,
        stripped: true,
        tableVariant: 'dark',
      },
      BTableLite: {
        footVariant: 'dark',
        headVariant: 'dark',
        hover: true,
        noBorderCollapse: true,
        stickyHeader: true,
        stripped: true,
        tableVariant: 'dark',
      },
      BTab: {
        variant: 'dark',
      },
      BTabs: {
        pills: true,
      },
      BToast: {
        variant: 'dark',
      },
    },
  },
}
