export function toast(t, title, message, variant = 'primary', extraProps = {}) {
  return t.$bvToast.toast(message, {
    title,
    variant,
    ...extraProps,
  })
}
