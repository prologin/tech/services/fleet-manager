import * as toast from './toast.js'

export function handleError(t) {
  return (err) => {
    toast.toast(t, 'An error occured', err.message, 'danger')
  }
}
