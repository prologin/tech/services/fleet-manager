export default async function (context) {
  if (context.$auth.loggedIn) {
    return
  }
  try {
    await context.$auth.loginWith('prologin', {})
  } catch (err) {
    console.log(err)
  }
}
