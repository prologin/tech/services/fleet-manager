#!/usr/bin/env bash

set -eo pipefail

getsecret() {
  FILE_PATH="$(eval "echo -n \"\${${1}_FILE}\"")"
  if [ -n "${FILE_PATH}" ]; then
    cat "${FILE_PATH}"
  else
    eval "echo -n \"\${${1}}\""
  fi
}

DB_POWERDNS_USER="$(getsecret "DB_POWERDNS_USER")"
DB_POWERDNS_PASSWORD="$(getsecret "DB_POWERDNS_PASSWORD")"
export DB_POWERDNS_USER DB_POWERDNS_PASSWORD

envsubst < /etc/powerdns/pdns.conf.tmpl > /etc/powerdns/pdns.conf

if [ "$#" -eq 0 ]; then
  set -- pdns_server
fi

exec "$@"
