#!/bin/sh

set -e

getsecret() {
  FILE_PATH="$(eval "echo -n \"\${${1}_FILE}\"")"
  if [ -n "${FILE_PATH}" ]; then
    cat "${FILE_PATH}"
  else
    eval "echo -n \"\${${1}}\""
  fi
}

DB_SALT_USER="$(getsecret "DB_SALT_USER")"
DB_SALT_PASSWORD="$(getsecret "DB_SALT_PASSWORD")"
SALT_API_SHARED_SECRET="$(getsecret "SALT_API_SHARED_SECRET")"
export DB_SALT_USER DB_SALT_PASSWORD SALT_API_SHARED_SECRET
envsubst < /etc/salt/master.tmpl > /etc/salt/master
unset DB_SALT_USER DB_SALT_PASSWORD SALT_API_SHARED_SECRET

mkdir -p /etc/salt/pki/master
getsecret "SALT_PKI_MASTER_PRIVATE_KEY" > /etc/salt/pki/master/master.pem
getsecret "SALT_PKI_MASTER_PUBLIC_KEY" > /etc/salt/pki/master/master.pub

if [ -n "$DEV" ] && [ "$#" -eq 0 ]; then
  salt-api &
fi

if [ "$#" -eq 0 ]; then
  set -- salt-master
fi

exec "$@"
