#!/bin/sh

set -e

getsecret() {
  FILE_PATH="$(eval "echo -n \"\${${1}_FILE}\"")"
  if [ -n "${FILE_PATH}" ]; then
    cat "${FILE_PATH}"
  else
    eval "echo -n \"\${${1}}\""
  fi
}

DB_KEA_USER="$(getsecret "DB_KEA_USER")"
DB_KEA_PASSWORD="$(getsecret "DB_KEA_PASSWORD")"
export DB_KEA_USER DB_KEA_PASSWORD

envsubst < /etc/kea/kea-dhcp4.conf.tmpl > /etc/kea/kea-dhcp4.conf
envsubst < /etc/kea/kea-ctrl-agent.conf.tmpl > /etc/kea/kea-ctrl-agent.conf

init() {
  echo "Checking if the database exists..."
  if ! kea-admin db-version pgsql -h "${DB_KEA_HOST}" -P "${DB_KEA_PORT:-5432}" -n "${DB_KEA_NAME}" -u "${DB_KEA_USER}" -p "${DB_KEA_PASSWORD}"; then
    echo "Database is not initialized. Initializing now..."
    export PGPASSWORD="${DB_KEA_PASSWORD}"
    psql --set ON_ERROR_STOP=1 -A -t -h "${DB_KEA_HOST}" -p "${DB_KEA_PORT:-5432}" -q -U "${DB_KEA_USER}" -d "${DB_KEA_NAME}" \
      -f /usr/share/kea/scripts/pgsql/dhcpdb_create.pgsql
    echo "Done."
  else
    echo "Database is already initialized."
  fi

  echo "Upgrading database..."
  kea-admin db-upgrade pgsql -h "${DB_KEA_HOST}" -P "${DB_KEA_PORT:-5432}" -n "${DB_KEA_NAME}" -u "${DB_KEA_USER}" -p "${DB_KEA_PASSWORD}"
  echo "Done."
}

config_discovery() {
  KEA_SHARED_SECRET="$(getsecret "KEA_SHARED_SECRET")"

  while true; do
    arguments="$(curl -fsSL \
      -H "Kea-Shared-Secret: ${KEA_SHARED_SECRET}" \
      "${DHCP_CONFIG_DISCOVERY_ENDPOINT}")"

    cat <<-EOF | curl -sLf -X POST -H "Content-Type: application/json" -d @- "${DHCP_CA_ENDPOINT:-http://localhost:8000/}"
      {
        "command": "config-set",
        "service": ["dhcp4"],
        "arguments": ${arguments}
      }
EOF
    echo

    sleep "${DHCP_CONFIG_DISCOVERY_INTERVAL:-60}"
  done
}

if [ -n "$INIT_JOB" ] || { [ -n "$DEV" ] && [ "$#" -eq 0 ]; }; then
  init
fi

if [ "$#" -eq 0 ]; then
  echo "Checking configuration..."
  kea-dhcp4 -t /etc/kea/kea-dhcp4.conf
  echo "Done."
  set -- kea-dhcp4 \
    -c /etc/kea/kea-dhcp4.conf \
    -p "${DHCP_SERVER_PORT:-67}" \
    -P "${DHCP_CLIENT_PORT:-68}"
elif [ "$#" -eq 1 ]; then
  if [ "${1}" = "control-agent" ]; then
    echo "Checking configuration..."
    kea-ctrl-agent -t /etc/kea/kea-ctrl-agent.conf
    echo "Done."
    set -- kea-ctrl-agent \
      -c /etc/kea/kea-ctrl-agent.conf
  elif [ "${1}" = "config-discovery" ]; then
    config_discovery
  fi
fi

exec "$@"
